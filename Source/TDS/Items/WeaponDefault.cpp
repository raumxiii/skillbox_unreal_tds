// Fill out your copyright notice in the Description page of Project Settings.
#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	ShellEjectLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShellEjectLocation"));
	ShellEjectLocation->SetupAttachment(RootComponent);

	MagazineDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("MagazineDropLocation"));
	MagazineDropLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (GetWeaponRound() > 0)
	{
		if (WeaponFiring && !WeaponReloading)
		{
			if (FireTimer < 0.f)
				Fire();
			else
				FireTimer -= DeltaTime;
		}
	}
	else if (!WeaponReloading)
		InitReload();
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
			FinishReload();
		else
			ReloadTimer -= DeltaTime;
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
			CurrentDispersion = CurrentDispersionMin;
		else if(CurrentDispersion > CurrentDispersionMax)
			CurrentDispersion = CurrentDispersionMax;
	}

	if (ShowDebug)
		UE_LOG(LogTemp,
			   Warning,
			   TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"),
			   CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
		SkeletalMeshWeapon->DestroyComponent(true);

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
		StaticMeshWeapon->DestroyComponent();

	UpdateStateWeapon(EMovementState::Run_State);
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
	{
		WeaponFiring = false;
		FireTimer = 0.01f;
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}


FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;
}

void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh,
	                              UArrowComponent* DropLocation,
	                              float LifeSpan,
	                              float ImpulsePower,
	                              float CustomMass,
	                              float ImpulseRandAngle)
{
    if (!DropMesh || !DropLocation)
		return;

	FTransform SpamTransform;
	FActorSpawnParameters SpawnParams;
	AStaticMeshActor* DropActor = nullptr;

	SpamTransform.SetLocation(DropLocation->GetComponentLocation());
	SpamTransform.SetRotation(DropLocation->GetComponentRotation().Quaternion());

	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnParams.Owner = GetOwner();

	DropActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(),
		                                                 SpamTransform,
		                                                 SpawnParams);

	if (DropActor && DropActor->GetStaticMeshComponent())
	{
		DropActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		DropActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		DropActor->SetActorTickEnabled(false);
		DropActor->InitialLifeSpan = LifeSpan;

		DropActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		DropActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		DropActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

		if (CustomMass > 0.f)
			DropActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			

		FVector FinalDirection = SpamTransform.GetRotation().Vector();
		if (!FinalDirection.IsNearlyZero())
		{
			if(!FMath::IsNearlyZero(ImpulseRandAngle))
				FinalDirection += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(FinalDirection, ImpulseRandAngle);
			FinalDirection.GetSafeNormal(0.0001f);

			DropActor->GetStaticMeshComponent()->AddImpulse(FinalDirection * ImpulsePower);
		}
	}
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSettings.RateOfFire;
	WeaponInfo.Round = WeaponInfo.Round - 1;
	ChangeDispersionByShot();

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		UGameplayStatics::SpawnSoundAtLocation(GetWorld(),
			                                   WeaponSettings.SoundFireWeapon,
			                                   ShootLocation->GetComponentLocation());

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
		{
			EndLocation = GetFireEndLocation();

			FVector Dir = EndLocation - SpawnLocation;

			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();

			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
				                                     WeaponSettings.EffectFireWeapon,
				                                     SpawnLocation,
				                                     SpawnRotation);

			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile,
					                                                                               &SpawnLocation,
					                                                                               &SpawnRotation,
					                                                                               SpawnParams));
				if (myProjectile)
					myProjectile->InitProjectile(WeaponSettings.ProjectileSettings);
			}
			else
			{
				FHitResult HitResult;
				TArray<AActor*> ActorList;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(),
													  SpawnLocation,
													  EndLocation * WeaponSettings.DistacneTrace,
													  ETraceTypeQuery::TraceTypeQuery4,
													  false,
													  ActorList,
													  EDrawDebugTrace::ForDuration,
													  HitResult,
													  true,
													  FLinearColor::Gray,
													  FLinearColor::Yellow,
													  6.f);
				if (ShowDebug)
					DrawDebugLine(GetWorld(),
						          SpawnLocation,
						          SpawnLocation + ShootLocation->GetForwardVector() * WeaponSettings.DistacneTrace,
						          FColor::Cyan,
						          false,
						          4.f,
						          (uint8)'\000',
						          0.5f);

				if (HitResult.GetActor() && HitResult.PhysMaterial.IsValid())
				{
					EPhysicalSurface HitSurfaceType = UGameplayStatics::GetSurfaceType(HitResult);
					if (WeaponSettings.ProjectileSettings.HitDecals.Contains(HitSurfaceType))
					{
						UMaterialInterface* HitMaterial = WeaponSettings.ProjectileSettings.HitDecals[HitSurfaceType];
						if (HitMaterial && HitResult.GetComponent())
							UGameplayStatics::SpawnDecalAttached(HitMaterial,
								 								 FVector(20.f),
								 								 HitResult.GetComponent(),
								  								 NAME_None,
									 							 HitResult.ImpactPoint,
																 HitResult.ImpactNormal.Rotation(),
																 EAttachLocation::KeepWorldPosition,
																 10.f);
					}

					if (WeaponSettings.ProjectileSettings.HitFXs.Contains(HitSurfaceType))
					{
						UParticleSystem* HitParticle = WeaponSettings.ProjectileSettings.HitFXs[HitSurfaceType];
						if (HitParticle)
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
								                                     HitParticle,
								                                     FTransform(HitResult.ImpactNormal.Rotation(),
																		        HitResult.ImpactPoint,
																		        FVector(1.f)));
					}

					if (WeaponSettings.ProjectileSettings.HitSound)
						UGameplayStatics::PlaySoundAtLocation(GetWorld(),
							                                  WeaponSettings.ProjectileSettings.HitSound,
							                                  HitResult.ImpactPoint);

					UGameplayStatics::ApplyDamage(HitResult.GetActor(),
												  WeaponSettings.ProjectileSettings.ProjectileDamage,
												  GetInstigatorController(),
												  this,
												  NULL);
				}
			}
		}
	}

	if (IsAim && WeaponSettings.AnimCharAimFire)
		OnWeaponFireStart.Broadcast(WeaponSettings.AnimCharAimFire);
	else if (WeaponSettings.AnimCharFire)
		OnWeaponFireStart.Broadcast(WeaponSettings.AnimCharFire);

	if (WeaponSettings.AnimFire && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSettings.AnimFire);

	InitDropMesh(WeaponSettings.ProjectileSettings.ShellBullets,
		         ShellEjectLocation,
		         WeaponSettings.ShellEjectLifeSpan, 
		         WeaponSettings.ShellEjectImpulsePower,
		         NULL,
		         30.f);

}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	//ToDo Dispersion
	BlockFire = false;

	switch (NewMovementState)
	{
		case EMovementState::Aim_State:
			CurrentDispersionMax = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMax;
			CurrentDispersionMin = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMin;
			CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimRecoil;
			CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
			IsAim = true;
			break;

		case EMovementState::AimWalk_State:
			CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimMax;
			CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimMin;
			CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
			CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
			IsAim = true;
			break;

		case EMovementState::Walk_State:
			CurrentDispersionMax = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMax;
			CurrentDispersionMin = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMin;
			CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimRecoil;
			CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
			IsAim = false;
			break;

		case EMovementState::Run_State:
			CurrentDispersionMax = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMax;
			CurrentDispersionMin = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMin;
			CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Run_StateDispersionAimRecoil;
			CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
			IsAim = false;
			break;

		case EMovementState::SprintRun_State:
			BlockFire = true;
			IsAim = false;
			SetWeaponStateFire(false);//set fire trigger to false
			//Block Fire
			break;
		default:
			break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() +
			          ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(),
				          ShootLocation->GetComponentLocation(),
				          -(ShootLocation->GetComponentLocation() - ShootEndLocation),
				          WeaponSettings.DistacneTrace,
				          GetCurrentDispersion() * PI / 180.f,
				          GetCurrentDispersion() * PI / 180.f,
				          32,
				          FColor::Emerald,
				          false,
				          .1f,
				          (uint8)'\000',
				          1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(),
				          ShootLocation->GetComponentLocation(),
				          ShootLocation->GetForwardVector(),
				          WeaponSettings.DistacneTrace,
				          GetCurrentDispersion() * PI / 180.f,
				          GetCurrentDispersion() * PI / 180.f,
				          32,
				          FColor::Emerald,
				          false,
				          .1f,
				          (uint8)'\000',
				          1.0f);
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(),
			          ShootLocation->GetComponentLocation(),
			          ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f,
			          FColor::Cyan,
			          false,
			          5.f,
			          (uint8)'\000',
			          0.5f);

		//direction projectile must fly
		DrawDebugLine(GetWorld(),
			          ShootLocation->GetComponentLocation(),
			          ShootEndLocation,
			          FColor::Red,
			          false,
			          5.f,
			          (uint8)'\000',
			          0.5f);

		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(),
			          ShootLocation->GetComponentLocation(),
			          EndLocation,
			          FColor::Black,
			          false,
			          5.f,
			          (uint8)'\000',
			          0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}

	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	if (WeaponReloading)
		return;

	WeaponReloading = true;
	ReloadTimer = WeaponSettings.ReloadTime;

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundReloadWeapon, ShootLocation->GetComponentLocation());

	//ToDo Anim reload
	if (WeaponSettings.AnimCharReload)
		OnWeaponReloadStart.Broadcast(WeaponSettings.AnimCharReload);

	if (WeaponSettings.AnimReload && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSettings.AnimReload);

	InitDropMesh(WeaponSettings.MagazineDrop,
				 MagazineDropLocation,
				 WeaponSettings.MagazineDropLifeSpan,
		         WeaponSettings.MagazineDropImpulsePower);
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	WeaponInfo.Round = WeaponSettings.MaxRound;

	OnWeaponReloadEnd.Broadcast();
}